import concurrent.futures
import requests
import cache
from flask import Flask, request, render_template
app = Flask(__name__)

@app.route('/')
def home():
  return render_template('demo.html')

@app.route('/', methods=['POST'])
def demo():
  city = request.form['city']
  checkin = request.form['checkin']
  checkout = request.form['checkout']
  results = get_hotel_rates(city, checkin, checkout)
  output = get_output_data(results)
  return render_template('output.html', data=output)

def get_hotel_rates(city, checkin, checkout):
	providers = [
		'snaptravel',
		'retail'
	]
	data = {
		'city': city,
		'checkin': checkin,
		'checkout': checkout
	}
	results = make_concurrent_requests(data, providers)
	return results

def make_concurrent_requests(data, providers):
	results = {}
	with concurrent.futures.ThreadPoolExecutor(max_workers=5) as executor:
		future_to_provider = {executor.submit(make_request, data, provider): provider for provider in providers}
		for future in concurrent.futures.as_completed(future_to_provider):
			provider = future_to_provider[future]
			try:
				res = future.result()
			except Exception as exc:
				print('%r generated an exception: %s' % (provider, exc))
			else:
				results[provider] = res
	return results

def make_request(data, provider):
	data['provider'] = provider
	endpoint = 'https://experimentation.snaptravel.com/interview/hotels'
	res = cache.get(data)
	if res:
		return res
	else:
		r = requests.post(endpoint, json=data)
		cache.put(data, r.json())
		return r.json()

def get_output_data(results):
	results_map = {} # provider -> hotel_id -> hotel
	id_set_map = {} # provider -> hotel_id set
	
	providers = [
		'snaptravel',
		'retail'
	]

	for p in providers:
		provider_hotel_ids = set()
		provider_results = {}
		for hotel in results[p]['hotels']:
			hotel_id = hotel['id']
			provider_results[hotel_id] = hotel
			provider_hotel_ids.add(hotel_id)

		id_set_map[p] = provider_hotel_ids
		results_map[p] = provider_results

	snap_hotel_ids = id_set_map['snaptravel']
	retail_hotel_ids = id_set_map['retail']

	common_ids = snap_hotel_ids.intersection(retail_hotel_ids)

	outputs = []
	for hotel_id in common_ids:
		snap_hotel = results_map['snaptravel'][hotel_id]
		retail_hotel = results_map['retail'][hotel_id]
		snap_price = snap_hotel['price']
		retail_price = retail_hotel['price']

		output = snap_hotel
		output['snap_price'] = snap_price
		output['retail_price'] = retail_price
		outputs.append(output)

	return outputs
