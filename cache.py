
in_memory_cache = {}

def get(request):
	key = get_key_from_request(request)
	return in_memory_cache.get(key, None)

def put(request, response):
	key = get_key_from_request(request)
	in_memory_cache[key] = response

def get_key_from_request(request):
	city = request['city']
	checkin = request['checkin']
	checkout = request['checkout']
	provider = request['provider']
	return city+checkin+checkout+provider