Requires Python v3.2 and above (https://docs.python.org/3/library/concurrent.futures.html)


To run the service please cd into your local repo directory and run:
```
pip install -r requirements.txt
export FLASK_APP=app.py
python -m flask run
```
